// console.log("Hello World")

// [SECTION] Assignment Operator
	// Basic Assignment Operator (=)
	// The assignment operator assigns the value od the right hand opereand to a variable
	let assignmentNumber = 8;
	console.log("The value of the assignmentNumber variable: " + assignmentNumber)
	console.log("-----------");

// [SECTION] Arithmetic Operations

let x = 200;
let y = 18;

console.log("x: " + x);
console.log("y: " + y);
console.log("-----------");

// Addition
let sum = x + y;
console.log("Result of addition opetor: " + sum);

// Subtraction
let difference = x - y;
console.log("Result of subtraction operator: " + difference);

// Multiplication
let product = x * y;
console.log("Result of multiplication operator: " + product);

// Division
let quotient = x / y;
console.log("Result of division operator: " + quotient);

// Remainder/Modulo
let modulo = x % y;
console.log("Result of modulo operator: " + modulo);

// Continuation of assignment operator

// Addition Assignment Operator
/*	Syntax: assignmentNumber = assignmentNumber + 		2; // long method
	console.log(assignmentNumber)*/

	// short method
	assignmentNumber += 2;
	console.log("Result of Addition Assinment Operator: " + assignmentNumber); //assignmentNumber value: 10


// Subtraction Assignment Operator
	/*// long method
	assignmentNumber = assignmentNumber - 3;
	console.log(assignmentNumber);*/

	assignmentNumber -= 3;
	console.log("Result of Subtraction Assinment Operator: " + assignmentNumber);

// Multiplication Assignment Operator
	assignmentNumber *=2;
	console.log("Result of Multiplication Assinment Operator: " + assignmentNumber);

// Division Assignment Operator
	assignmentNumber /=2;
	console.log("Result of Division Assinment Operator: " + assignmentNumber);

// [SECTION] PEMDAS (Order of Operations)
// Multiple Operators and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5
console.log(mdas)
	/*
		The operations were done in the following order:
		1. 3 * 4 = 12 | 1 + 2 - 12 / 5
		2. 12 / 5 = 2.4 | 1 + 2 - 2.4
		3. 1 + 2 = 3 | 3 - 2.4
		4. 3 - 2.4 = 0.6
	*/

	let pemdas = (1+(2-3)) * (4/5);
	console.log(pemdas)

	// Heirarchy
	// Combinations of multiple operators will follow pemdas rule.
		/*
		1. parenthesis
		2. exponent
		3. multiplication or division
		4. addition or subtraction

		NOTE: will also follow left to right rule
		*/

		// [SECTION] Increment and Decrement
		// Increment - increasing
		// Decrement - decreasing

		// Operators that add or substract values by 1 and reassign the value of the variable where the increment(++)/decrement(--) was applied.

let z = 1;

// pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment); // 2
console.log("Result of pre-increment of z: " + z); // 2

// post-increment
increment = z++;
console.log("Result of post-increment: " + increment); // 3
console.log("Result of post-increment of z: " + z); // 3
/*increment = z++;
console.log("Result of post-increment: " + increment); // 3
console.log("Result of post-increment of z: " + z); // 4*/

let decrement = --z;
console.log("Result of pre-decrement: " + decrement); // 2
console.log("Result of pre-decrement: " + z); // 2

decrement = z--;
console.log("Result of post-decrement: " + decrement); // 2
console.log("Result of post-decrement: " + z); // 1

// [SECTION] Type Coercion - is the automatic conversion of values from one data type to another


// combination of number and string data type will result to string
let numA = 10; // number
let numB = "12"; // string

let coercion = numA+numB;
console.log(coercion)
console.log(typeof coercion);

// combination of number and number data type will result to number
let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion)

// combination of number and boolean data type will result to number
let numX = 10;
let numY = true;
	// boolean values are
		// true = 1;
		// false = 0;
coercion = numX + numY;
console.log(coercion);
console.log(typeof coercion);

let num1 = 1;
let num2 = false;
coercion = num1 + num2;
console.log(coercion);
console.log(typeof coercion);

// [SECTION] Comparison Operators
// Comparison operators are used to evaluate and compare the left and right operands.
// After evaluation, it returns a boolean value

	// equality operator "=="
	// compares the value, but not the data type
console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == '1'); // true
console.log(0 == false); // true

let juan = "juan"
console.log('juan' == 'juan'); // true
console.log('juan' == 'Juan'); // false // equality is strict with letter casing.
console.log(juan == "juan"); // true

console.log("-----------------")

// inqequality operator "!="
console.log(1 != 1); // false
console.log(1 != 2); // true
console.log(1 != '1'); // false
console.log(0 != false); // false
console.log('juan' != 'juan'); // false
console.log('juan' != 'Juan'); // true // equality is strict with letter casing.
console.log(juan != "juan"); // false

console.log("-----------------")


// [SECTION] Relational Operators
// Some comparison operators check wheteher one value id greater or less than the other value
// It will return a value of true or false

let a = 50;
let b = 65;

// GT or Greater Than Operator (>)
let isGreaterThan = a > b; // 50 > 65 = false
console.log(isGreaterThan);

// LT or Less Than Operator (>)
let isLessThan = a < b; // 50 < 65 = true
console.log(isLessThan);

// GTE or Greater Than or Equal (<=)
let isGTorEqual = a >= b; // false
console.log(isGTorEqual);

// LTE or Less Than or Equal (<=)
let isLTorEqual = a <= b; // true
console.log(isLTorEqual);

// Forced Coercion
let num = 56;
let numStr = "30";
console.log(num > numStr); // true

let str = "twenty";
console.log(num >= str) // false
// Since the string is not numeric, the string will not be converted to a number, or a.k.a. NaN (Not a Number)

console.log("-----------------")

// Logical AND Operator (&&)
let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical AND Operator: " + allRequirementsMet);
	
	// AND operator requires all / both are true

// Logical OR Operator (||)
let someRequirementsMet = isLegalAge || isRegistered; // true
console.log("Result of Logical OR Operator: " + someRequirementsMet); // OR operator requires only 1 true;

// Logical Not Operator (!)
// Returns the opposite value
let someRequirementsNotMet = !isLegalAge;
console.log("Result of Logical NOT Operator: " + someRequirementsNotMet)